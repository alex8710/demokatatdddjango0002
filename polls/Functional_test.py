from __future__ import absolute_import
from unittest import TestCase

# import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
# from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
# from selenium.webdriver.support.select import Select


class FunctionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome('E:\\MISO UNIANDES\\Procesos Agiles\\Kata Web\\Chrome driver\\chromedriver.exe')

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://127.0.0.1:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_registro(self):
        self.browser.implicitly_wait(3)
        self.browser.get('http://127.0.0.1:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Steven Alexander')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Erira')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.send_keys('5')

        self.browser.find_element_by_xpath('//select[@id="id_tiposDeServicio"]/option[text()="Desarrollador Web"]').click()

        self.browser.implicitly_wait(3)
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3108526168')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('sa.erira@uniandes.edu.co')

        imagen = self.browser.find_element_by_id('id_imagen')
        imagen.send_keys('C:\\foto.jpg')

        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys('alex8710')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('contrasena')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)
        span = self.browser.find_element(By.XPATH, '//span[text()="Steven Alexander Erira"]')
        self.assertIn('Steven Alexander Erira', span.text)

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)
        span = self.browser.find_element(By.XPATH, '//span[text()="Steven Alexander Erira"]')
        self.assertIn('Steven Alexander Erira', span.text)

    def test_verDetalle(self):
        self.browser.get('http://127.0.0.1:8000')
        span = self.browser.find_element(By.XPATH, '//span[text()="Steven Alexander Erira"]')
        span.click()

        h2 = self.browser.find_element(By.XPATH, '//h2[text()="Steven Alexander Erira"]')
        self.assertIn('Steven Alexander Erira', h2.text)

    def test_login(self):
        self.browser.implicitly_wait(3)
        self.browser.get('http://127.0.0.1:8000')
        link = self.browser.find_element_by_id('id_login')
        link.click()

        log_user = self.browser.find_element_by_id('login_usuario')
        log_user.send_keys('alex8710')

        pwd_user = self.browser.find_element_by_id('login_password')
        pwd_user.send_keys('contrasena')

        botonLogin = self.browser.find_element_by_id('login_ingresar')
        botonLogin.click()
        self.browser.implicitly_wait(3)

        logged_user = self.browser.find_element_by_id('id_editar')  # logged_user

        self.assertIn('alex8710', logged_user.text)

    def test_editar(self):
        self.browser.implicitly_wait(3)
        self.browser.get('http://127.0.0.1:8000')

        link = self.browser.find_element_by_id('id_login')
        link.click()

        log_usuario = self.browser.find_element_by_id('login_usuario')
        log_usuario.send_keys('alex8710')

        password = self.browser.find_element_by_id('login_password')
        password.send_keys('contrasena')

        login = self.browser.find_element_by_id('login_ingresar')
        login.click()

        alertid = self.browser.find_element_by_id('alertid')
        alertid.click()

        editar = self.browser.find_element_by_id('id_editar')
        editar.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.clear()
        nombre.send_keys('Leonel')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.clear()
        apellidos.send_keys('Messi')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.clear()
        experiencia.send_keys('2')

        self.browser.find_element_by_xpath('//select[@id="id_tiposDeServicio"]/option[text()="Desarrollador Web"]').click()

        self.browser.implicitly_wait(3)
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.clear()
        telefono.send_keys('3008443276')

        correo = self.browser.find_element_by_id('id_correo')
        correo.clear()
        correo.send_keys('ha.torres11@uniandes.edu.co')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()

        logout = self.browser.find_element_by_id('logoutId')
        logout.click()

        span = self.browser.find_element(By.XPATH, '//span[text()="Leonel Messi"]')
        span.click()

        h2 = self.browser.find_element(By.XPATH, '//h2[text()="Leonel Messi"]')
        self.assertIn('Leonel Messi', h2.text)


    def test_comentario(self):
        self.browser.implicitly_wait(3)
        self.browser.get('http://127.0.0.1:8000')
        span = self.browser.find_element(By.XPATH, '//span[text()="Leonel Messi"]')
        span.click()
        correo = self.browser.find_element_by_id('correo')
        correo.send_keys('sa.erira@uniandes.edu.co')
        comentario = self.browser.find_element_by_id('comentario')
        comentario.send_keys('Prueba comentarios')
        boton = self.browser.find_element_by_id('RegistrarCommentario')
        boton.click()